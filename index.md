---
layout: frontpage
header: FalkirkLUG is now TechMeetup Stirling!
---

FalkirkLUG is now TechMeetup Stirling! If you're looking for a group of people
to hang out with, talk tech, have a meal, and watch a talk with, then come
check us out!

TechMeetup Stirling is free to attend, so come along and see for yourself! We
meet on the 1st Tuesday of every month, at CodeBase Stirling (in the Hotdesking
area). We meet from about 6.30pm, talk at ~7pm and then on to a local pub for
food, drink, and chat.

You can find out all about the upcoming TechMeetup events, and come chat with
us if you have any questions, on our forum:

[https://community.techmeetup.co.uk](https://community.techmeetup.co.uk)

See you there?
